from getConn import connection
from flask import Flask , render_template, request, url_for, redirect, jsonify, json
import json
from random import *
app = Flask(__name__)

imageList = []
canList =[]

@app.route('/table/<string:choosen>/<string:correct>')
def table(choosen,correct):
	listChoosen = choosen.split('|')
	listCorrect = correct.split('|')
	return render_template('table.html',size = 10, choosen = listChoosen,correct = listCorrect,imageList=imageList,canList = canList)

@app.route('/game')
def first():
	destData = json.loads(getDestData())
	rows = 10
	cols = 11
	questions=[]
	for row in range(rows): 
		questions += [[0]*cols]

	i=0
	for row in destData:
		questions[i][0] = row['attractionName']
		questions[i][1] = row['option1']
		questions[i][2] = row['option2']
		questions[i][3] = row['option3']
		questions[i][4] = row['attractionImageURL']
		questions[i][5] = row['attractionCanonical']
		questions[i][6] = questions[i][0]
		questions[i][7] = row['placeCode']
		questions[i][8] = row['placeCode1']
		questions[i][9] = row['placeCode2']
		questions[i][10] = row['placeCode3']
		i+=1
	i=0	
	del imageList[:]
	del canList[:]
	for row in range(rows):
		x = randint(0, 3)
		questions[i][0] = questions[i][x]
		questions[i][x] = questions[i][6] 
		
		temp = questions[i][x+7]					#swap values for placeCode
		questions[i][x+7] = questions[i][7]
		questions[i][7] = temp 
		
		imageList.append(questions[i][4])
		canList.append(questions[i][5])
		i+=1
	"""attraction_namesList = []
	questionList = []
	option2List = []
	option3List = []
	for row in destData:
		attraction_namesList.append(row['attraction_name'])
		option1List.append(row['option1'])
		option2List.append(row['option2'])
		option3List.append(row['option3'])
	"""
	return render_template('game1.html',size = 10,questions=questions)

def first_upper(s):
   if len(s) == 0:
      return s
   else:
      return s[0] + s[1:].lower()

def getDestData():
	c, conn = connection()
	query = ("SELECT attractionName,option1,option2,option3,attractionImageURL,attractionCanonical,placeCode,placeCode1,placeCode2,placeCode3 From temp_attr order by rand() limit 10")
	c.execute(query)
	results = c.fetchall()
	obj = []
	for row in results:
		dic = {
			'attractionName' : str(row[0]),
			'option1' : str(row[1]),
			'option2' : str(row[2]),
			'option3' : str(row[3]),
			'attractionImageURL' : str(row[4]),
			'attractionCanonical' : str(row[5]),
			'placeCode' : first_upper(str(row[6])),
			'placeCode1' : first_upper(str(row[7])),
			'placeCode2' : first_upper(str(row[8])),
			'placeCode3' : first_upper(str(row[9]))}
		obj.append(dic)
	return	json.dumps(obj)

if __name__ == '__main__':
   app.run(debug = True)